package chat

import (
	"chat/dao"
	"chat/model"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
)

var (
	UserIds []int
	mu      sync.Mutex
)

func InitUserSet() {
	UserIds = make([]int, model.MaxUserNums)
}

func GetUserId() (res int64, err error) {
	mu.Lock()
	defer mu.Unlock()
	for i := 0; i < len(UserIds); i++ {
		if UserIds[i] == 0 {
			UserIds[i] = 1
			return int64(i), nil
		}
	}
	return -1, errors.New("room is full")
}

func SetUserId(id int64) {
	mu.Lock()
	defer mu.Unlock()
	UserIds[id] = 0
}

type message struct {
	TimeStamp int64  `json:"time_stamp"`
	UserName  string `json:"user_name"`
	Msg       string `json:"msg"`
}

var upGrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func ConnRoom(c *gin.Context) {
	defer fmt.Println("Conn api close")
	// prepare check
	roomId := c.Request.FormValue("room_id")
	userName := c.Request.FormValue("user_name")
	if roomId == "" || userName == "" {
		return
	}

	room_id, err := strconv.Atoi(roomId)
	if err != nil {
		log.Println(err)
		return
	}

	room := model.ChatRoom(int64(room_id))
	if room.UserNum >= model.MaxUserNums {
		return
	}

	uid, err := GetUserId()
	if err != nil {
		return
	}

	//upgrade websocket
	ws, err := upGrader.Upgrade(c.Writer, c.Request, nil)
	if err != nil {
		log.Println(err.Error())
		return
	}
	defer ws.Close()

	// init var
	ch := make(chan string)

	// set close func hook
	ws.SetCloseHandler(func(code int, text string) error {
		room.RemoveUser(uid)
		SetUserId(uid)
		return nil
	})

	// subscribe room
	room.Subscribe(uid, ch)

	done := make(chan struct{})

	//send msg to others
	go func(ws *websocket.Conn) {
		defer close(done)
		for {
			msgtype, msgbyte, err := ws.ReadMessage()
			if err != nil || msgtype == -1 {
				return
			}
			room.Publish(userName, string(msgbyte))
		}
	}(ws)

	for {
		palpitate := time.NewTicker(30 * time.Second)
		// when ch closed, return to terminate the goroutine
		select {
		case msg := <-ch:
			if msg == "" {
				return
			}

			var response []message
			strs := strings.Split(msg, "#!#")
			timeStamp, _ := strconv.Atoi(strs[0])
			response = append(response, message{
				TimeStamp: int64(timeStamp),
				UserName:  strs[1],
				Msg:       strs[2],
			})
			err := ws.WriteJSON(response)
			if err != nil {
				return
			}
		case <-palpitate.C:
			err := ws.WriteJSON(nil)
			if err != nil {
				return
			}
		case <-done:
			room.RemoveUser(uid)
			SetUserId(uid)
			return
		}
	}
}

func ChatHistory(c *gin.Context) {
	room_id := c.Request.FormValue("room_id")

	// sync already exsit data
	res := dao.RDB.ZRevRange(model.RoomId+room_id, 0, 1000)
	msgs, _ := res.Result()

	var response []message
	for _, v := range msgs {
		strs := strings.Split(v, "#!#")
		timeStamp, _ := strconv.Atoi(strs[0])
		response = append(response, message{
			TimeStamp: int64(timeStamp),
			UserName:  strs[1],
			Msg:       strs[2],
		})
	}
	c.JSON(http.StatusOK, response)
}
