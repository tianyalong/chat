package chat

import (
	"chat/model"
	"net/http"
	"runtime"

	"github.com/gin-gonic/gin"
)

func Ping(c *gin.Context) {
	room := model.ChatRoom(1)
	c.JSON(http.StatusOK, gin.H{
		"Goroutine nums: ": runtime.NumGoroutine(),
		"Users nums: ":     room.UserNum,
	})
}
