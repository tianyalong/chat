package dao

import (
	"log"
	"time"

	"github.com/go-redis/redis"
)

// RDB 声明一个全局的rdb变量
var RDB *redis.Client

// InitRedisClient 初始化单机redis-cli连接
func InitRedisClient() {
	RDB = redis.NewClient(&redis.Options{
		Addr:         "172.26.191.209" + ":6379",
		Password:     "", // no password set
		DB:           0,  // use default DB
		DialTimeout:  time.Duration(1000) * time.Millisecond,
		ReadTimeout:  time.Duration(1000) * time.Millisecond,
		WriteTimeout: time.Duration(1000) * time.Millisecond,
	})

	_, err := RDB.Ping().Result()
	if err != nil {
		log.Println("redis启动失败")
	}

	// go CleanChat()
}

func CleanChat() {
	ticket := time.NewTicker(time.Minute)

	for {
		res := RDB.ZCard("room_id:1")
		if cnt, _ := res.Result(); cnt > 1000 {
			log.Println("rmrange:", cnt-1001)
			RDB.ZRemRangeByRank("room_id:1", 0, cnt-1001)
		}
		<-ticket.C
	}
}
