package main

import (
	"chat/chat"
	"chat/dao"
	"chat/myutil"
	"log"
)

func main() {
	// test code
	// go func() {
	// 	for {
	// 		room := model.ChatRoom(1)
	// 		fmt.Println(room.UserNum, room.Users, runtime.NumGoroutine())
	// 		time.Sleep(time.Second)
	// 	}
	// }()
	myutil.InitLog()
	dao.InitRedisClient()
	chat.InitUserSet()

	if err := InitRoute().Run(":7006"); err != nil {
		log.Panic(err.Error())
	}
}
