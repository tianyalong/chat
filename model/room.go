package model

import (
	"chat/dao"
	"errors"
	"strconv"
	"sync"
	"time"

	"github.com/go-redis/redis"
)

var (
	MaxUserNums int64 = 200
	mu          sync.Mutex
	rooms       = make(map[int64]*chatRoom)
)

type chatRoom struct {
	sync.Mutex
	roomId  int64
	UserNum int64
	Users   map[int64]chan<- string
}

// reutrn a room, create a room if not exist
func ChatRoom(id int64) *chatRoom {
	mu.Lock()
	defer mu.Unlock()

	if room, ok := rooms[id]; ok {
		return room
	}

	room := &chatRoom{
		roomId: id,
		Users:  make(map[int64]chan<- string),
	}
	rooms[id] = room
	return room
}

// delete room from rooms
func DeleteRoom(id int64) {
	mu.Lock()
	defer mu.Unlock()

	if _, ok := rooms[id]; !ok {
		return
	}

	delete(rooms, id)
}

// add a user into room
func (r *chatRoom) Subscribe(userId int64, userCh chan<- string) (err error) {
	if r.UserNum >= MaxUserNums {
		return errors.New("room is full")
	}

	r.Lock()
	defer r.Unlock()

	if r.UserNum >= MaxUserNums {
		return errors.New("room is full")
	}

	if _, ok := r.Users[userId]; ok {
		return errors.New("repeat subscribe")
	}
	(*r).Users[userId] = userCh
	(*r).UserNum++

	return nil
}

// send msg to all users
func (r *chatRoom) Publish(userName string, msg string) {
	score := float64(time.Now().UnixNano())
	member := strconv.Itoa(int(time.Now().UnixNano())) + "#!#" + userName + "#!#" + msg
	r.Lock()
	for _, ch := range r.Users {
		select {
		case ch <- member:
		case <-time.After(time.Second):
		}
	}
	r.Unlock()

	dao.RDB.ZAdd(RoomId+strconv.Itoa(int(r.roomId)), redis.Z{
		Score:  score,
		Member: member,
	})
}

// close the room, remove all users
func (r *chatRoom) CloseRoom() {
	r.Lock()
	defer r.Unlock()

	for uid, ch := range r.Users {
		delete(r.Users, uid)
		close(ch)
	}
	r.UserNum = 0
}

// remove user by id
func (r *chatRoom) RemoveUser(uid int64) {
	r.Lock()
	defer r.Unlock()

	ch, ok := r.Users[uid]
	if !ok {
		return
	}

	close(ch)
	delete(r.Users, uid)
	r.UserNum--
}
