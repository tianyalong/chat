package myutil

import (
	"log"
	"os"
)

func InitLog() {
	//设置时区
	err := os.Setenv("TZ", "Asia/Shanghai")
	if err != nil {
		log.Println(err.Error())
		return
	}
	logFile, err := os.OpenFile("./static/Logs.log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0644)
	if err != nil {
		log.Println("log初始化失败")
		return
	}

	log.SetOutput(logFile)
	log.SetFlags(log.Llongfile | log.Lmicroseconds | log.Ldate)
}
