package main

import (
	"chat/chat"

	easygomiddleware "git.acwing.com/1318686278/easygo/middleware"
	"github.com/gin-gonic/gin"
)

var handlers = map[string]gin.HandlerFunc{
	"/ping":    chat.Ping,
	"/conn":    chat.ConnRoom,
	"/history": chat.ChatHistory,
}

func InitRoute() *gin.Engine {
	preFn := func(prefix string, url string, handle func(c *gin.Context)) func(c *gin.Context) {
		return func(c *gin.Context) {
			handle(c)
		}
	}

	gin.SetMode(gin.ReleaseMode)
	r := gin.Default()
	r.Use(easygomiddleware.SetHeader)

	// 第一期接口
	{
		prefix := "/chat"
		v1 := r.Group(prefix)
		for k, v := range handlers {
			handler := preFn(prefix, k, v)
			v1.GET(k, handler)
			v1.POST(k, handler)
			v1.OPTIONS(k, handler)
		}
	}
	return r
}
